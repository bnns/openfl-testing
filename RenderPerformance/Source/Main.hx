package;

import openfl.display.DisplayObject;
import test.IPerformanceTest;
import haxe.rtti.Meta;
import lime.app.Application;
import openfl.system.Capabilities;
import lime.system.System;
import openfl.text.TextField;
import openfl.text.TextFormat;
import lime.ui.KeyCode;
import openfl.events.KeyboardEvent;
import openfl.events.Event;
import openfl.display.FPS;
import openfl.display.Sprite;

import debug.Memory;
import utils.Key;
import test.SimpleGraphicTest;
import test.SimpleBitmapTest;
import test.SimpleTilemapTest;
import test.SimpleStarlingTest;


class Main extends Sprite
{
	private var container:Sprite;
	private var _fps:FPS;
	private var _memory:Memory;
	private var _objectCount:TextField; 
	private var platform:TextField;
	private var _results:TextField;
	
	private var isAutoTest:Bool = false;

	private var currentTest:IPerformanceTest;
	private var currentTestIndex:Int = 0;
	private var testList:Array<IPerformanceTest> = [
		new SimpleGraphicTest(),
		new SimpleBitmapTest(),
		new SimpleTilemapTest(),
		new SimpleStarlingTest()
	];
	private var testTimeNoChange:Int = 0;

	public function new()
	{
		super();
		stage.scaleMode = NO_SCALE;
		Key.initialize(stage);
		
		addChild(container=new Sprite());
		addChild(_fps=new FPS());
		addChild(_memory=new Memory());
		addChild(platform = new TextField());
		addEventListener(Event.ENTER_FRAME, onUpdate);
		
		//_fps.defaultTextFormat = new TextFormat("Arial",12,null, true);
		platform.x = _fps.x;
		
		var textHeight:Float = 16;
		var fpsDTF = _fps.defaultTextFormat;
		fpsDTF.bold = false;
		platform.defaultTextFormat = fpsDTF;
		platform.text = "Capabilities: os ["+Capabilities.os+"] arch ["+Capabilities.cpuArchitecture+"] manufacturer ["+Capabilities.manufacturer+"] version ["+Capabilities.version+"] lang ["+Capabilities.language+"]";
		
		var fields = Type.getClassFields(Capabilities);
		for (key in fields){
			
			if(key.indexOf("get_")>-1)key=key.split("get_")[1];
			var value = Reflect.getProperty(Capabilities,key);
			trace("key: "+key+" value: "+value);
			//if(Reflect.isFunction(value)){
				//trace("func: "+key+" value: "+Reflect.callMethod(Capabilities,cast(value, haxe.Constraints.Function),new Array<Dynamic>()));
				
			//}
		}

		platform.width = stage.stageWidth;
		platform.wordWrap = true;
		platform.height = textHeight;
		_fps.height = textHeight;
		_fps.y = platform.height; 

		_memory.height = textHeight;
		_memory.y = _fps.y+_fps.height; 
		_memory.x = _fps.x;

		_objectCount = new TextField();
		_objectCount.defaultTextFormat = fpsDTF;
		_objectCount.height = textHeight;
		_objectCount.y = _memory.y+_memory.height;
		_objectCount.x = _memory.x;
		addChild(_objectCount);

		_results = new TextField();
		_results.defaultTextFormat = fpsDTF;
		_results.wordWrap = true;
		_results.y = _objectCount.y+_objectCount.height;
		_results.height = stage.stageHeight-_results.y;
		_results.width = stage.stageWidth;
		_results.x = _objectCount.x;
		addChild(_results);
		

		#if mobile
			isAutoTest = true;
		#end

	}
	
	

	private function onUpdate(event:Event):Void
	{
		var targetTest:IPerformanceTest = null;
		var targetTestIndex:Int = -1;
		if(Key.IsPressed(KeyCode.NUMBER_1)){
			targetTestIndex = 0;
		}
		if(Key.IsPressed(KeyCode.NUMBER_2)){
			targetTestIndex = 1;
		}
		if(Key.IsPressed(KeyCode.NUMBER_3)){
			targetTestIndex = 2;
		}
		if(Key.IsPressed(KeyCode.NUMBER_4)){
			targetTestIndex = 3;
		}
		if(Key.IsPressed(KeyCode.A)){
			isAutoTest = !isAutoTest;
		}

		if(isAutoTest && currentTest == null){
			targetTestIndex = ++currentTestIndex;
		}

		if(currentTest != null){
			_objectCount.text = "Count: "+ currentTest.GetObjectsCount();
			if(currentTest.Evaluate(_fps.currentFPS, 30))
			{
				testTimeNoChange++;
				if(isAutoTest && testTimeNoChange>= 30 *5 && currentTestIndex < testList.length){
					targetTestIndex = ++currentTestIndex;
					_results.text+= Type.getClassName(Type.getClass(currentTest))+": "+currentTest.GetObjectsCount()+"\n";
					testTimeNoChange = 0;

					if(currentTestIndex >= testList.length){
						isAutoTest = false;
					}
				}
			} else {
				testTimeNoChange = 0;
			}
			
		}

		var testInstance = testList[targetTestIndex];
		if( testInstance != null){
			var testType = Type.getClass(testInstance);
			targetTest = Type.createInstance(testType,[]);
		}

		if(targetTest != null){
			
			if(currentTest != null)
			{
				if(Std.is(currentTest, DisplayObject))
					container.removeChild(cast(currentTest,DisplayObject));
				currentTest.Terminate();
			}
			currentTest = targetTest;

			if(Std.is(currentTest, DisplayObject))
			{
				container.addChild(cast(currentTest,DisplayObject));
				currentTest.Initialize();
			}
		}
	}

}
