package debug;

import haxe.Timer;
import openfl.text.TextFormat;
import openfl.system.System;
import openfl.events.Event;
import openfl.text.TextField;

class Memory extends TextField
{
    public var PeakMemory:Float = 0;
    public var CurrentMemory:Float = 0;

    private  var _memoryList:Array<{mem:Float,time:Float}> = new Array<{mem:Float,time:Float}>();

    public function new(x:Float = 10, y:Float = 10, color:Int = 0x000000) {
        super();

        addEventListener(Event.ADDED_TO_STAGE, OnAdded);
        addEventListener(Event.REMOVED_FROM_STAGE, OnRemoved);


        this.x = x;
		this.y = y;

		selectable = false;
		mouseEnabled = false;
        defaultTextFormat = new TextFormat("_sans", 12, color);

        width = 250;        
    }

    private function OnAdded(e:Event):Void {
        removeEventListener(Event.ENTER_FRAME, OnUpdate);
        addEventListener(Event.ENTER_FRAME, OnUpdate);
    }
    private function OnRemoved(e:Event):Void {
        removeEventListener(Event.ENTER_FRAME, OnUpdate);
    }
    private function OnUpdate(e:Event):Void {
        CurrentMemory = Math.round(System.totalMemory / 1024 / 1024 * 100)/100;

        if(CurrentMemory > PeakMemory){
            PeakMemory = CurrentMemory; 
        }

        var now = Timer.stamp();
        _memoryList.push({mem:CurrentMemory,time:now});

        while (_memoryList.length > 0 && _memoryList[0].time < now-1)
            _memoryList.shift();

        var avgMemory:Float = 0;
        if(_memoryList.length > 0){
            var i:Int = 0;
            for ( i in 0..._memoryList.length)
            {
                avgMemory += _memoryList[i].mem;
            }
            avgMemory = Math.round((avgMemory/_memoryList.length) * 100)/100;
        }
        text = "Mem: "+avgMemory+" Peak: "+PeakMemory;
    }

}