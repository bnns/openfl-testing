package utils;

import haxe.ds.Map;
import lime.ui.KeyCode;
import openfl.utils.Dictionary;
import openfl.events.Event;
import openfl.events.KeyboardEvent;
import openfl.display.FPS;
import openfl.display.Sprite;
import openfl.display.Stage;

class Key
{
    private function new() {
        
    }

    static private var _stage:Stage;
    static private var _keysChanged:Map<UInt, Bool> = new Map<UInt, Bool>(); 
    static private var _keysCurrent:Map<UInt, Bool> = new Map<UInt, Bool>();
    
    public static function initialize(stage:Stage):Void
    {
        if(_stage != null){
            _stage.removeEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
            _stage.removeEventListener(KeyboardEvent.KEY_UP, OnKeyUp);
            _stage.removeEventListener(Event.FRAME_CONSTRUCTED, OnUpdate);
        }

        _stage = stage;
        _stage.addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
        _stage.addEventListener(KeyboardEvent.KEY_UP, OnKeyUp);
        _stage.addEventListener(Event.FRAME_CONSTRUCTED, OnUpdate, false, 1000);
        
    }

    private static function OnKeyDown(event:KeyboardEvent):Void
    {
        _keysChanged[event.keyCode] = true;
    }

    private static function OnKeyUp(event:KeyboardEvent):Void
    {
        _keysChanged[event.keyCode] = false;
    }

    private static function OnUpdate(event:Event):Void {
        
        for (k => v in _keysChanged){
            _keysCurrent.set(k,v);
            _keysChanged.remove(k);
        }
        
    }

    /**
        is the specifed button pressed this frame
    **/
    public static function IsPressed(key:KeyCode):Bool
    {
        return !_keysCurrent[key] && _keysChanged[key];
    }
        
    /**
        is the specifed button held down
    **/
    public static function IsDown(key:KeyCode):Bool
    {
        return _keysCurrent[key] || _keysChanged[key];
    }


    /**
        is the specifed button released this frame
    **/
    public static function IsReleased(key:KeyCode):Bool
    {
        return !_keysChanged[key];
    }

}