package test;

import starling.display.MeshBatch;
import openfl.display3D.Context3DProfile;
import openfl.display3D.Context3DRenderMode;
import starling.utils.Align;
import openfl.events.TimerEvent;
import openfl.utils.Timer;
import starling.display.Image;
import openfl.geom.Point;
import openfl.display.BitmapData;
import openfl.geom.Rectangle;
import starling.textures.Texture;
import starling.display.DisplayObjectContainer;
import openfl.events.Event;
import openfl.display.Sprite;
import starling.core.Starling;

class SimpleStarlingTest extends Sprite implements IPerformanceTest
{
    private var _starling:Starling;

    public function new() {
        super();
    }

    public function Initialize():Void
    {
        if(stage == null){
            addEventListener(Event.ADDED_TO_STAGE, OnAdded);
        } else {
            OnAdded(null);
        }
    }

    private function OnAdded(e:Event):Void
    {
        removeEventListener(Event.ADDED_TO_STAGE, OnAdded);
        /**
        "enhanced",
        "standardExtended", "standard", "standardConstrained",
        "baselineExtended", "baseline", "baselineConstrained"
        **/
        _starling = new Starling(RootStarlingContainer, stage, null, null, Context3DRenderMode.AUTO, "auto");
        //_starling.supportHighResolutions = true;
        _starling.addEventListener(starling.events.Event.ROOT_CREATED, OnRootCreated);
        _starling.showStatsAt(Align.RIGHT);
        _starling.start();
    }

    private function OnRootCreated(e:starling.events.Event):Void
    {
        _starling.removeEventListener(starling.events.Event.ROOT_CREATED, OnRootCreated);
        cast(_starling.root, RootStarlingContainer).Initialize();
    }
    /**
        returns true if it's within acceptable parameters
    **/
    public function Evaluate(currentFPS:Int, minFPS:Int):Bool
    {
        if(_starling == null || _starling.root == null){
            return true;
        } 

        return cast(_starling.root, RootStarlingContainer).Evaluate(currentFPS, minFPS);
    }
    public function GetObjectsCount():Float{
        
        if(_starling == null || _starling.root == null){
            return 0;
        } 

        return cast(_starling.root, RootStarlingContainer).GetObjectsCount();
    }
    public function Terminate():Void
    {
        if(_starling != null){
            if(_starling.root != null){
                cast(_starling.root, RootStarlingContainer).Terminate();
            }
            _starling.dispose();
        }
        _starling = null;
        
        removeEventListener(Event.ADDED_TO_STAGE, OnAdded);
    }
    

}

class RootStarlingContainer extends starling.display.Sprite implements IPerformanceTest {

    private var _container:starling.display.Sprite = new starling.display.Sprite();
    private var _hasPassedMinFPS:Bool = false;
    private var _list:Array<TestRenderItem> = new Array<TestRenderItem>();

    private var _mainTexture:Texture;
    private var _regions:Array<Rectangle> = new Array<Rectangle>(); 
    

    public function new() {
        super();
    }    

    public function Initialize():Void
    {
        touchable = false;
        addChild(_container);
        var totalSize:UInt = 256;
        var bmpd:BitmapData = new BitmapData(totalSize,totalSize,true,0);
        var size:UInt = 32;
        var width:Int = Math.floor(totalSize/size);
        var height:Int = Math.floor(totalSize/size);
        
        for (i in 0...width)
        {
            var j:Int = 0;
            for(j in 0...height)
            {
                var region:Rectangle = new Rectangle(i*size, j*size, size, size);
                _regions.push(region);

                var border:Int = 2;
                region.x+=border;
                region.y+=border;
                region.width-=border*2;
                region.height-=border*2;
                
                bmpd.fillRect(region,Math.floor((0xFFFFFF)*Math.random())+(Math.floor(0xFF*Math.random())<<24));
                //bmpd.fillRect(region,Math.floor((0xFFFFFFFF)*Math.random()));
                
            }
        }
        _mainTexture = Texture.fromBitmapData(bmpd);
        
        //Texture.fromTexture
        
        addEventListener(starling.events.Event.ENTER_FRAME, OnEnterframe);
    }

    
    /**
        returns true if it's within acceptable parameters
    **/
    public function Evaluate(currentFPS:Int, minFPS:Int):Bool
    {   
        _hasPassedMinFPS = (currentFPS <= minFPS);
        
        return _hasPassedMinFPS;
        
    }

    public function addTestItems(amount:Int=1):Void
    {
        for(i in 0...amount){
            var size:Int = Math.floor(Math.random()*16+16);
            //var color:UInt = Math.floor(Math.random()*(0xffffff));
            //if(_primaryTexture == null){
                _primaryTexture = Texture.fromTexture(_mainTexture, _regions[Math.floor(_regions.length*Math.random())], null, false);
            //}
            var image:Image = new Image(_primaryTexture);
            image.alignPivot();
            image.pixelSnapping = false;

            //bmp.alpha = 1;
            var logic:TestRenderItem = new TestRenderItem();
            logic.velocity = new Point(5*Math.random(),0);
            logic.render = image;
            logic.gravity += logic.gravity*(size/24); 
            _list.push(logic);
            _container.addChild(image);
            //addChild(image);
        }
    }

    private var _primaryTexture:Texture;
    private function OnEnterframe(e:starling.events.Event):Void 
    {
        if(!_hasPassedMinFPS){
            addTestItems(10);
        }

        for (logic in _list){
            logic.velocity.y += logic.gravity;
            if(logic.position.x < 0 && logic.velocity.x < 0) 
                logic.velocity.x = logic.velocity.x*-1;  
            if(logic.position.x > stage.stageWidth && logic.velocity.x > 0) 
                logic.velocity.x = logic.velocity.x*-1;
            if(logic.position.y > stage.stageHeight && logic.velocity.y > 0){ 
                logic.velocity.y = logic.velocity.y*-1;
                logic.velocity.y = logic.velocity.y*Math.random()*.1+logic.velocity.y*.9;
            }

            logic.rotation = Math.atan2(logic.velocity.y,logic.velocity.x);
            logic.position.x += logic.velocity.x;
            logic.position.y += logic.velocity.y;
            
            var render:Image = cast(logic.render, Image);
            render.rotation = logic.rotation;
            render.x = logic.position.x;
            render.y = logic.position.y;
        }
        //addChild(_container);
    }

    public function GetObjectsCount():Float{
        return _container.numChildren;
    }
    public function Terminate():Void
    {
        _list = null;
        _regions.splice(0,_regions.length);
        _regions = null;
        removeChildren();
        removeEventListeners();
        _mainTexture.dispose();
        _mainTexture = null;
        
    }
    
}