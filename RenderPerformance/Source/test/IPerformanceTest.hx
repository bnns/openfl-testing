package test;

interface IPerformanceTest {
    function Initialize():Void;

    /**
        returns true if it's within acceptable parameters
    **/
    function Evaluate(currentFPS:Int, minFPS:Int):Bool;
    function GetObjectsCount():Float;
    function Terminate():Void;
}