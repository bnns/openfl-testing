package test;

import openfl.geom.Rectangle;
import openfl.display.Tileset;
import openfl.display.Tile;
import openfl.display.Tilemap;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.geom.Point;
import openfl.Vector;
import openfl.display.Shape;
import openfl.events.Event;
import openfl.display.Sprite;

class SimpleTilemapTest extends Sprite implements IPerformanceTest {

    private var _hasPassedMinFPS:Bool = false;
    private var _list:Vector<TestRenderItem> = new Vector<TestRenderItem>();
    private var _tileset:Tileset = null;
    private var _tilemap:Tilemap = null;
    public function Initialize():Void
    {
        var rects:Array<Rectangle> = new Array<Rectangle>();
        var bmpd:BitmapData = new BitmapData(2048,2048,true);
        var size:UInt = 32;
        var width:Int = Math.floor(1024/size);
        var height:Int = Math.floor(1024/size);
        

        //var i:Int = 0;
        for (i in 0...width)
        {
            var j:Int = 0;
            for(j in 0...height)
            {
                var region:Rectangle = new Rectangle(i*size, j*size, size, size);
                rects.push(region);
                bmpd.fillRect(region,Math.floor((0xFFFFFF)*Math.random()+(Math.floor(0xFF*Math.random())<<24)));

            }
        }

        _tileset = new Tileset(bmpd, rects);
        _tilemap = new Tilemap(stage.stageWidth, stage.stageHeight, _tileset, true);
        _tilemap.tileAlphaEnabled = true;
        _tilemap.tileBlendModeEnabled = false;
		_tilemap.tileColorTransformEnabled = false;
        addChild(_tilemap);
        addEventListener(Event.ENTER_FRAME, onUpdate);
    }

    public function Evaluate(currentFPS:Int, minFPS:Int):Bool
    {
        _hasPassedMinFPS = (currentFPS <= minFPS);
        
        return _hasPassedMinFPS;
    }

    public function addTestItems(amount:Int=1):Void
    {
        for(i in 0...amount){
            var size:Int = Math.floor(Math.random()*16+16);
            var color:UInt = Math.floor(Math.random()*(0xffffff));
            var tile:Tile = new Tile(Math.floor(_tileset.numRects*Math.random()));
            
            var logic:TestRenderItem = new TestRenderItem();
            logic.velocity = new Point(5*Math.random(),0);
            logic.render = tile;
            logic.gravity += logic.gravity*(size/24); 
            _list.push(logic);
            _tilemap.addTile(tile);
        }
    }

    public function onUpdate(e:Event):Void
    {
        if(!_hasPassedMinFPS){
           addTestItems(10);
        }
        
        for (logic in _list){
            logic.velocity.y += logic.gravity;
            if(logic.position.x < 0 && logic.velocity.x < 0) 
                logic.velocity.x = logic.velocity.x*-1;  
            if(logic.position.x > stage.stageWidth && logic.velocity.x > 0) 
                logic.velocity.x = logic.velocity.x*-1;
            if(logic.position.y > stage.stageHeight && logic.velocity.y > 0){ 
                logic.velocity.y = logic.velocity.y*-1;
                logic.velocity.y = logic.velocity.y*Math.random()*.1+logic.velocity.y*.9;
            }

            logic.rotation = Math.atan2(logic.velocity.y,logic.velocity.x)*(180/Math.PI);
            logic.position.x += logic.velocity.x;
            logic.position.y += logic.velocity.y;
            
            var render:Tile = cast(logic.render, Tile);
            render.rotation = logic.rotation;
            render.x = logic.position.x;
            render.y = logic.position.y;
        }
    }

    public function GetObjectsCount():Float
    {
        return _tilemap.numTiles;
    }

    public function Terminate():Void
    {
        _tileset.bitmapData.dispose();
        _tileset.bitmapData = null;
        _tileset.rectData.length = 0;
        _tilemap.removeTiles();
        _tilemap.tileset = null;
        removeEventListener(Event.ENTER_FRAME, onUpdate);
        removeChildren();
        _list.length = 0;

    }
}