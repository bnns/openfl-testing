package test;

import openfl.utils.Object;
import openfl.geom.Point;

class TestRenderItem {
    public function new() {
        
    }

    public var position:Point = new Point();
    public var offset:Point = new Point();
    public var rotation:Float = 0;
    public var velocity:Point = new Point();
    public var gravity:Float = 0.098;

    public var render:Any;
}