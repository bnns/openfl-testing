package test;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.geom.Point;
import openfl.Vector;
import openfl.display.Shape;
import openfl.events.Event;
import openfl.display.Sprite;

class SimpleBitmapTest extends Sprite implements IPerformanceTest {

    private var _hasPassedMinFPS:Bool = false;
    private var _list:Vector<TestRenderItem> = new Vector<TestRenderItem>();

    public function Initialize():Void
    {
        mouseChildren = false;
        mouseEnabled = false;
        addEventListener(Event.ENTER_FRAME, onUpdate);
    }

    public function Evaluate(currentFPS:Int, minFPS:Int):Bool
    {
        _hasPassedMinFPS = (currentFPS <= minFPS);
        
        return _hasPassedMinFPS;
    }

    public function addTestItems(amount:Int=1):Void
    {
        for(i in 0...amount){
            var size:Int = Math.floor(Math.random()*16+16);
            var color:UInt = Math.floor(Math.random()*(0xffffff));
            var bmpd:BitmapData = new BitmapData(size,size,false,color);
            var bmp:Bitmap = new Bitmap(bmpd);
            
            bmp.alpha = 1;
            var logic:TestRenderItem = new TestRenderItem();
            logic.velocity = new Point(5*Math.random(),0);
            logic.render = bmp;
            logic.gravity += logic.gravity*(size/24); 
            _list.push(logic);
            addChild(bmp);
        }
    }

    public function onUpdate(e:Event):Void
    {
        if(!_hasPassedMinFPS){
            addTestItems(5);
        }
        
        for (logic in _list){
            logic.velocity.y += logic.gravity;
            if(logic.position.x < 0 && logic.velocity.x < 0) 
                logic.velocity.x = logic.velocity.x*-1;  
            if(logic.position.x > stage.stageWidth && logic.velocity.x > 0) 
                logic.velocity.x = logic.velocity.x*-1;
            if(logic.position.y > stage.stageHeight && logic.velocity.y > 0){ 
                logic.velocity.y = logic.velocity.y*-1;
                logic.velocity.y = logic.velocity.y*Math.random()*.1+logic.velocity.y*.9;
            }
            logic.rotation = Math.atan2(logic.velocity.y,logic.velocity.x)*(180/Math.PI);
            logic.position.x += logic.velocity.x;
            logic.position.y += logic.velocity.y;
            
            var render:Bitmap = cast(logic.render, Bitmap);
            render.rotation = logic.rotation;
            render.x = logic.position.x;
            render.y = logic.position.y;
        }
    }

    public function GetObjectsCount():Float
    {
        return numChildren;
    }

    public function Terminate():Void
    {
        removeEventListener(Event.ENTER_FRAME, onUpdate);
        removeChildren();
        _list.length = 0;

    }
    
}